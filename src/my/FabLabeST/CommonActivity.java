package my.FabLabeST;

import my.FabLabeST.PreferenceComponents.MesPreferences;

import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;

public class CommonActivity extends Activity {
	public boolean onKeyDown(int keyCode, KeyEvent event) { 
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            //do your work
        	 Intent i = new Intent(CommonActivity.this, MesPreferences.class);
             startActivityForResult(i, 0  );
             return true;
        }
        return super.onKeyDown(keyCode, event); 
    } 
}
