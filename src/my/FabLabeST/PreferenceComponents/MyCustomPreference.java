package my.FabLabeST.PreferenceComponents;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.util.AttributeSet;

/**
 * Created by Arnaud on 07/11/14.
 */
public class MyCustomPreference extends Preference {
    final String deviceName ="my.FabLabeST.Prefs.BluetoothDevice";
    final String deviceID="my.FabLabeST.Prefs.BluetoothDeviceID";
    public MyCustomPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MyCustomPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCustomPreference(Context context) {
        super(context);
    }

    public synchronized void setDevice(String address,String name) {
        SharedPreferences sharedPref = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(deviceName /*getString(R.string.saved_high_score)*/, address);
        editor.putString(deviceID /*getString(R.string.saved_high_score)*/, name);
        editor.commit();
        notify();


    }

    public String getDeviceName() {
        SharedPreferences sharedPref = getSharedPreferences();
        return sharedPref.getString(/*getString(R.string.saved_high_score)*/deviceName, "undef");
    }
    public String getDeviceID() {
        SharedPreferences sharedPref = getSharedPreferences();
        return sharedPref.getString(/*getString(R.string.saved_high_score)*/deviceID, "undef");
    }

    public CharSequence getSummary() {
        final CharSequence deviceName = getDeviceName();
        final CharSequence deviceID = getDeviceID();
        final CharSequence summary = super.getSummary();
        if (summary == null || deviceName == null|| deviceID == null) {
            return null;
        } else {
            return String.format(summary.toString(), deviceName,deviceID);
        }
    }

}
