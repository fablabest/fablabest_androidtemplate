package my.FabLabeST.PreferenceComponents;


import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.preference.DialogPreference;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.LinearLayout;

import my.FabLabeST.GlobalData.Main;


public class MySeekBarPreference extends DialogPreference implements SeekBar.OnSeekBarChangeListener
{
  private static final String androidns="http://schemas.android.com/apk/res/android";

  private SeekBar mSeekBar;
  private TextView mSplashText,mValueText;
  private Context mContext;

  private String mDialogMessage, mSuffix;
  private int mDefault, mMax, mValue = 0;

  public MySeekBarPreference(Context context, AttributeSet attrs) {
    super(context,attrs); 
    mContext = context;

    mDialogMessage = attrs.getAttributeValue(androidns,"dialogMessage");
    mSuffix = attrs.getAttributeValue(androidns,"text");
    mDefault = attrs.getAttributeIntValue(androidns,"defaultValue", 0);
    mMax = attrs.getAttributeIntValue(androidns,"max", 100);

  }
  @Override 
  protected View onCreateDialogView() {
    LinearLayout.LayoutParams params;
    LinearLayout layout = new LinearLayout(mContext);
    layout.setOrientation(LinearLayout.VERTICAL);
    layout.setPadding(6,6,6,6);

    mSplashText = new TextView(mContext);
    if (mDialogMessage != null)
      mSplashText.setText(mDialogMessage);
    layout.addView(mSplashText);

    mValueText = new TextView(mContext);
    mValueText.setGravity(Gravity.CENTER_HORIZONTAL);
    mValueText.setTextSize(32);
    params = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.FILL_PARENT, 
        LinearLayout.LayoutParams.WRAP_CONTENT);
    layout.addView(mValueText, params);

    mSeekBar = new SeekBar(mContext);
    mSeekBar.setOnSeekBarChangeListener(this);
    layout.addView(mSeekBar, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

    if (shouldPersist())
      mValue = getPersistedInt(mDefault);

    mSeekBar.setMax(mMax);
    mSeekBar.setProgress(mValue);
    return layout;
  }
  @Override 
  protected void onBindDialogView(View v) {
    super.onBindDialogView(v);
    mSeekBar.setMax(mMax);
    mSeekBar.setProgress(mValue);
  }
  @Override
  protected void onSetInitialValue(boolean restore, Object defaultValue)  
  {
    super.onSetInitialValue(restore, defaultValue);
    if (restore) 
      mValue = shouldPersist() ? getPersistedInt(mDefault) : 0;
    else 
      mValue = (Integer)defaultValue;
    if (pcli!=null) pcli.changes(mValue);
      //setProgress(mValue);
  }


  public void onProgressChanged(SeekBar seek, int value, boolean fromTouch)
  {
   // String t = String.valueOf(((float)value)/10);
    //mValueText.setText(mSuffix == null ? t : t.concat(mSuffix));
    if (mSuffix == null) {
    	mValueText.setText("Gain non defini");
    } else {
    	 final CharSequence summary = super.getSummary();
    	mValueText.setText(String.format(summary.toString(),/*  getProgress()*/
    			((float)value)/10));
        if (pcli!=null) pcli.changes(value);
    }
    /*if (shouldPersist())
      persistInt(value);*/
    callChangeListener(new Integer(value));
    mValue=value;
  }
  public void onStartTrackingTouch(SeekBar seek) {}
  public void onStopTrackingTouch(SeekBar seek) {}

  public void setMax(int max) { mMax = max; }
  public int getMax() { return mMax; }

  public void setProgress(int progress) { 
    mValue = progress;
    if (mSeekBar != null)
      mSeekBar.setProgress(progress); 
   // notifyChanged();
  }
  public int getProgress() { return mValue; }
  
  @Override
  public CharSequence getSummary() {
      final CharSequence summary = super.getSummary();
      if (summary == null ) {
           return null;
      } else {
          return String.format(summary.toString(),/*  getProgress()*/
        		  ((float)getPersistedInt(mDefault))/10);
      }
  }
  public void onDialogClosed(boolean positiveResult) {
	  
	  if(positiveResult){
		//  persistString(volumeLevel.getProgress()+"");
		  persistInt( getProgress());
		  notifyChanged();

		 }
      if (Main.d) Log.i("SeekBarPref", "Persisted " + getPersistedInt(mDefault));
      if (pcli!=null) pcli.changes(getPersistedInt(mDefault));

  }



    // Report changes on the value selection
  public interface PropagatChangeListenerInterf {
      public void changes(int value);
  }

  PropagatChangeListenerInterf pcli;
  public void subscribePropagatChangeListenerInterf(PropagatChangeListenerInterf pcli) {
     this.pcli=pcli;
  }
}