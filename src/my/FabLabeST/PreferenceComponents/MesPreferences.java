package my.FabLabeST.PreferenceComponents;

import my.FabLabeST.GlobalData.Main;

import my.FabLabeST.R;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
//import android.widget.Toast;

//my.Equilibre.MesPreferences.MyListPreference

public class MesPreferences extends PreferenceActivity {
	


	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	addPreferencesFromResource(R.xml.preference);
	this.setTitle(R.string.settingTitleTxt);

    Main.m.getBlueToothMng().initProperties(this);

}


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Main.m.getBlueToothMng().onActivityResult(requestCode, resultCode,data);
    }

}
