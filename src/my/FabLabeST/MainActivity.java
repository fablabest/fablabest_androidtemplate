package my.FabLabeST;


import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

import my.FabLabeST.BlueTooth.BluetoothService;
import my.FabLabeST.BlueTooth.MyBlueToothMngInterf;
import my.FabLabeST.GlobalData.Main;

public class MainActivity extends CommonActivity implements MyBlueToothMngInterf {
    private final static String TAG = MainActivity.class.getCanonicalName();
    /** Called when the activity is first created. */


    Thread thread;

    ImageButton bls_transmit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Main.m.getBlueToothMng().setBLS_MyBlueToothMngInterf(this);
        setContentView(R.layout.bls_animation_gallery);

        Main.m.getBlueToothMng().onCreate();

    	this.setTitle("Animations "/*+CurrentPatientManaged.pi.toString()*/);


        bls_transmit = (ImageButton)  findViewById(R.id.bls_transmit);
        bls_transmit.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub
               /* int position = gallery.getSelectedItemPosition();
                if (position >= 0) {
                    String cmd ="";
                    cmd+=BLS_Main.m.getBlueToothMng().setupAtConnection(BLS_AnimationGalleryActivityBLS.this);
                    cmd+= BLS_Main.m.la.get(position).returnDeviceCommands();
                    if (BLS_Main.d) Log.i("info", "cmd:\n" + cmd);
                    BLS_Main.m.getBlueToothMng().sendData(cmd);
                }
*/
            }

        });

        refreshConnectionStateShown();


    }


    /* ***************************************
       ***  Bluetooth wrapper on the activity ***
       ***************************************
     */

    public void onStart() {
        super.onStart();
        Main.m.getBlueToothMng().onStart();
    }

    protected void onPause() {
        super.onPause();

    }

    protected void onResume() {
        super.onResume();

        Main.m.getBlueToothMng().onResume();
    }

    protected void onDestroy() {

        super.onDestroy();
        Main.m.getBlueToothMng().onDestroy();
        Main.m.getBlueToothMng().setBLS_MyBlueToothMngInterf(null);
    }

    /*** Part dedicated to the Bluetooth connection  ***/
    //Message regarding the bluetooth connection
    @Override
    public void int_connected(String device) {
        Toast.makeText(getApplicationContext(),"Connected to "+device,
                Toast.LENGTH_SHORT).show();
        refreshConnectionStateShown();
    }

    @Override
    public void int_connecting() {
        Toast.makeText(getApplicationContext(), " Connecting ...",
                Toast.LENGTH_SHORT).show();
        refreshConnectionStateShown();
    }

    @Override
    public void int_not_connected() {
        Toast.makeText(getApplicationContext(),"Device not connected",
                Toast.LENGTH_SHORT).show();
        refreshConnectionStateShown();
    }

    @Override
    public void int_bluetooth_device_not_available() {
        Toast.makeText(getApplicationContext(), "Device not available",
                Toast.LENGTH_SHORT).show();
    }


    @Override
    public void int_bluetooth_cannot_be_enabled() {
        Toast.makeText(getApplicationContext(), "Bluetooth cannot be enabled",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWheelInfo(String text) {
       /* if(measurewheelinfoET!=null) {
          if (text==null) measurewheelinfoET.setText("");
          else measurewheelinfoET.setText("Measure:"+text);
        }*/
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void message(String text) {
        Toast.makeText(getApplicationContext(), text,
                Toast.LENGTH_SHORT).show();
    }

    @Override
      public void ask_for_bluetooth_enabled() {
        Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableIntent, BluetoothService.REQUEST_ENABLE_BT);
    }

    @Override
    public void ask_for_bluetooth_disabled() {
      /*  Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISABLE);
        startActivityForResult(enableIntent, BLS_BluetoothService.REQUEST_ENABLE_BT);*/
    }

    @Override
    public void ask_for_ensure_discoverable() {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);
    }

    public void refreshConnectionStateShown() {
        bls_transmit.setEnabled(Main.m.getBlueToothMng().getBluetoothConnectionState()== BluetoothService.STATE_CONNECTED);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Main.m.getBlueToothMng().onActivityResult(requestCode, resultCode, data);
    }

        /* Activity Exiting management*/

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }
}