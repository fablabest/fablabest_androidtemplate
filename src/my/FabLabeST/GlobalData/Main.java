package my.FabLabeST.GlobalData;

import android.os.Environment;
import android.util.Log;
import my.FabLabeST.BlueTooth.BlueToothMng;


import java.io.File;

/**
 * Created by baixasa on 8/28/2014.
 */
public class Main {

    public static boolean d=false;


    public static Main m= new Main();

    private static final String LOG_TAG = "MyActivity";
    public File getAnimationStorageDir() {

        File imagefolder=null;
        try {
            File root = Environment.getExternalStorageDirectory();
            if (root.canWrite()) {
                imagefolder = new File(root,
                        "FabLabeST");
                imagefolder.mkdirs();
            } else
                Log.e(LOG_TAG, "Can not write in the directory " + root.getPath());
        } catch (Exception e) {
            Log.e("DEBUG", "Could not write file " + e.getMessage());
        }
        return imagefolder;
    }


    BlueToothMng bm=new BlueToothMng();

    public BlueToothMng getBlueToothMng() {
        /*if (bm==null)
            bm=new BLS_BlueToothMng();*/
        return bm;
    }

    public void prepareToFinish() {

    }

}
