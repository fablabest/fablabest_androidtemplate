package my.FabLabeST;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

/**
 * Created by baixasa on 10/31/2014.
 */
public class FirstAnimationActivity extends Activity {

    boolean alreadycreate=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bls_startanimation);

    }

    public void onResume() {
        super.onResume();
        if (! alreadycreate) {
            animstart();
            alreadycreate=true;
        } else {
            animend();
        }
    }

    public void animstart() {

        Animation titleFadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        ImageView titleicon = (ImageView)findViewById(R.id.bls_bikelighttitle);
        Animation animContestFadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        ImageView contesticon = (ImageView)findViewById(R.id.bls_contesttext);
        Animation animbody_icon2 = AnimationUtils.loadAnimation(this, R.anim.body_in);
        final ImageView bikebodyicon = (ImageView)findViewById(R.id.bls_bikebody);
        //ImageView titleicon = new ImageView(this);
        //titleicon.setImageResource(R.drawable.wheel);
        //titleicon.setVisibility();
        animbody_icon2.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub
                bikebodyicon.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                Intent intent = new Intent(FirstAnimationActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });

        titleicon.startAnimation(titleFadeout);
        contesticon.startAnimation(animContestFadeout);
        bikebodyicon.startAnimation(animbody_icon2);
    }

    public void animend() {

        Animation animbody_icon2 = AnimationUtils.loadAnimation(this, R.anim.body_out);
        final ImageView bikebodyicon = (ImageView)findViewById(R.id.bls_bikebody);
        //Animation mynameFadeout = AnimationUtils.loadAnimation(this, R.anim.fademyname);
     //   final ImageView mynameicon = (ImageView)findViewById(R.id.bls_myname);
/*
        mynameFadeout.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                //Intent intent = new Intent(BLS_FirstAnimationActivity.this, BLS_AnimationGalleryActivityBLS.class);
                //startActivity(intent);
                mynameicon.setVisibility(1);
                //bikebodyicon.setVisibility(0);
                BLS_FirstAnimationActivity.this.finish();
            }
        });*/

        animbody_icon2.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                //Intent intent = new Intent(BLS_FirstAnimationActivity.this, BLS_AnimationGalleryActivityBLS.class);
                //startActivity(intent);
                bikebodyicon.setVisibility(View.INVISIBLE);
                bikebodyicon.setAlpha(0);
                FirstAnimationActivity.this.finish();
            }
        });
     //   mynameicon.startAnimation(mynameFadeout);
        bikebodyicon.startAnimation(animbody_icon2);

    }
}
