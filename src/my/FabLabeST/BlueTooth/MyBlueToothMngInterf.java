package my.FabLabeST.BlueTooth;

import android.content.Context;

/**
 * Created by baixasa on 11/2/2014.
 */
public interface MyBlueToothMngInterf {
    public void int_connected(String device);
    public void int_connecting();
    public void int_not_connected();
    public void int_bluetooth_device_not_available();
    public void message(String text);
    public void ask_for_bluetooth_enabled();
    public void ask_for_ensure_discoverable();
    public void ask_for_bluetooth_disabled();
    public void int_bluetooth_cannot_be_enabled();
    public void showWheelInfo(String text);
    Context getContext();
}

