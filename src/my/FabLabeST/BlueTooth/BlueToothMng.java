package my.FabLabeST.BlueTooth;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.util.Log;

import my.FabLabeST.GlobalData.Main;
import my.FabLabeST.PreferenceComponents.MyCustomPreference;
import my.FabLabeST.PreferenceComponents.MySeekBarPreference;
//import android.widget.Toast;

public class BlueToothMng {


    private static final String TAG = "BLS_MyBlueToothMng";

    // My variable that must contain the bluetooth state
    public int BluetoothConnectionState= BluetoothService.STATE_NONE;
    public int BluetoothFeatureState= BluetoothService.STATE_BT_OFF;

    boolean D=false;

    private BluetoothAdapter mBluetoothAdapter = null;

    private BluetoothService bluetoothService = null;


    DataReceiptionRecognition drr=new DataReceiptionRecognition();

    public BlueToothMng()
    {}
	public BlueToothMng(MyBlueToothMngInterf btmi) {
        this.btmi=btmi;
    }

    public int getBluetoothConnectionState() {
        return BluetoothConnectionState;
    }


    public class DataReceiptionRecognition {

        int statemachin;
        int angle =0;
        int speed =0;
        boolean speednegative=false;


        public void initialise() {
            statemachin=0;
            angle=0;
            speed=0;
        }

        public boolean isDec(char c) {
            return ((c>='0')&&(c<='9'));
        }

        public void treatChar(char [] data) {

            for (char c:data)
            {
            	if (c==':') {
            		 // Start of a new line
            		statemachin=0;
            		angle =0;
            		speed =0;
                    speednegative=false;
            		//drawView.addPoint(intValue/20,10);
            	} else {statemachin++;}

            	switch(statemachin) {
            	 case 0:break;
                 case 1 : if (c!='A')  statemachin=0;break;
                 case 2 : if (c!='N')  statemachin=0;break;
                 case 3 : if (c!='G')  statemachin=0;break;
                 case 4 : if (c!='L')  statemachin=0;break;
                 case 5 : if (c!='E')  statemachin=0;break;
            	  // Recognize the format :######-######-######
            	 case 6:
                     if (!isDec(c))  {statemachin=0;break;}
                     angle *=10;
                     angle +=Character.digit(c,10);break;
            	 case 7:
                     if (!isDec(c))  {statemachin=0;break;}
                     angle *=10;
                     angle +=Character.digit(c,10);break;
            	 case 8:
                     if (!isDec(c))  {statemachin=0;break;}
                     angle *=10;
                     angle +=Character.digit(c,10);break;
                 case 9 : if (c!=' ')  statemachin=0;break;
                 case 10: if (c!='S')  statemachin=0;break;
                 case 11 :if (c!='P')  statemachin=0;break;
                 case 12: if (c!='E')  statemachin=0;break;
                 case 13 : if (c!='E')  statemachin=0;break;
                 case 14 : if (c!='D')  statemachin=0;break;
                 case 15 : if (c=='+')  speednegative=false;
                           else if (c=='-')  speednegative=true;
                           else statemachin=0;break;
            	 case 16:
                     if (!isDec(c))  {statemachin=0;break;}
                     speed =(int)Character.digit(c,10);break;
            	 case 17:
                     if (!isDec(c))  {statemachin=0;break;}
                     speed *=10;
                     speed +=Character.digit(c,10);break;
            	 case 18:
                     if (!isDec(c))  {statemachin=0;break;}
                     speed *=10;
                     speed +=Character.digit(c,10);
                     if (speednegative)
                         speed=-speed;
                     if (btmi!=null) btmi.showWheelInfo("Angle "+angle+" Speed"+speed);
                     break;
            	}
            }
        }
    };



    public void setBLS_MyBlueToothMngInterf(MyBlueToothMngInterf btmi) {
        this.btmi=btmi;
    }

    CheckBoxPreference activateBluetoothCBP;
	CheckBoxPreference enableDeviceConnectionCBP;
	CheckBoxPreference autoDisconnectDeviceCBP;
    MyCustomPreference bluetoothSelectDevice;

    // Specific settings
    MySeekBarPreference angularVerticalSensorOffset;
    MySeekBarPreference angularDisplayOffset;
    MySeekBarPreference matrixSpeedLimit;
    MySeekBarPreference realTimeParamsPeriod;


	boolean enabletimeout=false;
	
	PreferenceActivity pa;
	public void initProperties(final PreferenceActivity pa) {
		this.pa=pa;
        // Register for broadcasts on BluetoothAdapter state change
        //IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        //this.pa.registerReceiver(mReceiver, filter);
		// BLS_MyListPreference BTList = (BLS_MyListPreference)pa.findPreference("BluetoothAppareil");


       activateBluetoothCBP = (CheckBoxPreference) pa.findPreference("BluetoothActivated");

       activateBluetoothCBP.setOnPreferenceChangeListener( new OnPreferenceChangeListener(){
            public boolean onPreferenceChange(Preference pref, Object val) {
                // if (mpp!=null) mpp.setGridEnabled(val.toString().equals("true"));
                //showConnectionStart(ConnectionState.Ongoing);
                boolean state=val.toString().equals("true");
                if (state) {
                    if (mBluetoothAdapter==null) {
                     Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                     pa.startActivityForResult(enableIntent, BluetoothService.REQUEST_ENABLE_BT);
                    } else {
                        if (! mBluetoothAdapter.isEnabled()) mBluetoothAdapter.enable();
                    }
                    //contactToBluetoothAdapter();
                } else {
                    if ((mBluetoothAdapter!=null)&&(mBluetoothAdapter.isEnabled())) {
                        mBluetoothAdapter.disable();
                    }
                }

                return true;
            }
        });


        bluetoothSelectDevice = (MyCustomPreference) pa.findPreference("BluetoothSelectDevice");
        bluetoothSelectDevice.setEnabled((mBluetoothAdapter!=null)&&(mBluetoothAdapter.isEnabled()));
        bluetoothSelectDevice.setOnPreferenceClickListener( new Preference.OnPreferenceClickListener(){


            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent serverIntent = new Intent(pa, DeviceListActivity.class);
                pa.startActivityForResult(serverIntent, BluetoothService.REQUEST_CONNECT_DEVICE);
                return true;
            }
        });



	    enableDeviceConnectionCBP = (CheckBoxPreference) pa.findPreference("BluetoothEnabled");
        //enableDeviceConnectionCBP.setEnabled(bluetoothService!=null);
        //enableDeviceConnectionCBP.setEnabled((mBluetoothAdapter!=null)&&(mBluetoothAdapter.isEnabled()));
        BluetoothFeatureState=((mBluetoothAdapter!=null)&&(mBluetoothAdapter.isEnabled()))? BluetoothService.STATE_BT_ON: BluetoothService.STATE_BT_OFF;
	    enableDeviceConnectionCBP.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference pref, Object val) {

                boolean state = val.toString().equals("true");

                if (state) {//enableDeviceConnectionCBP.isChecked()

                    String address = bluetoothSelectDevice.getDeviceName();///"to set";//selectiondevice.getDeviceName();////////////////////////////////////////////////////////////To be defined !!!!!!!!!!!!!
                    // Get the BLuetoothDevice object
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    // Attempt to connect to the device
                    bluetoothService.connect(device);
                } else {
                    bluetoothService.stop();
                    //	 askForDisconnection();
                }

                return true;
            }
        });

	    autoDisconnectDeviceCBP = (CheckBoxPreference) pa.findPreference("AutoDisconnectDevice");
	    //enabletimeout=autoDisconnectDeviceCBP.isChecked();
	    autoDisconnectDeviceCBP.setOnPreferenceChangeListener( new OnPreferenceChangeListener(){
	         public boolean onPreferenceChange(Preference pref, Object val) {
                /* if (mBluetoothAdapter == null) return true;
                 Intent serverIntent = new Intent(pa, BLS_DeviceListActivity.class);
                 pa.startActivityForResult(serverIntent, BLS_BluetoothService.REQUEST_CONNECT_DEVICE);*/
/*
	        	boolean state=val.toString().equals("true");



	        	 if (state) {//enableDeviceConnectionCBP.isChecked()
	        		 timeout=TIMEOUTMAX;
	        		 enabletimeout=true;
	        	 } else {
	        		 enabletimeout=false;
	        	 }*/

	             return true;
	         }
	     });

        // Add All other properties
        /*
        angularVerticalSensorOffset =(BLS_MySeekBarPreference) pa.findPreference("AngularVerticalSensorOffset");
        angularVerticalSensorOffset.subscribePropagatChangeListenerInterf(new BLS_MySeekBarPreference.PropagatChangeListenerInterf() {

            @Override
            public void changes(int value) {
                sendMessage(setupAngleVerticalSensorOffset(value / 10));
            }
        });

        angularDisplayOffset =(BLS_MySeekBarPreference) pa.findPreference("AngularDisplayOffset");
        angularDisplayOffset.subscribePropagatChangeListenerInterf(new BLS_MySeekBarPreference.PropagatChangeListenerInterf() {

            @Override
            public void changes(int value) {
                sendMessage(setupAngleDisplayOffset(value / 10));
            }
        });

        matrixSpeedLimit=(BLS_MySeekBarPreference) pa.findPreference("MatrixSpeedLimit");
        matrixSpeedLimit.subscribePropagatChangeListenerInterf(new BLS_MySeekBarPreference.PropagatChangeListenerInterf() {

            @Override
            public void changes(int value) {
                sendMessage(setupMatrixSpeedLimit(value/10));
            }
        });*/

        realTimeParamsPeriod=(MySeekBarPreference) pa.findPreference("RealTimeParamsPeriod");
        realTimeParamsPeriod.subscribePropagatChangeListenerInterf(new MySeekBarPreference.PropagatChangeListenerInterf() {

            @Override
            public void changes(int value) {
                sendMessage(setupRealTimeWheelParams(value/10));
            }
        });

        refreshBluetoothPropertyButtons();
	}

    private String setupAngleVerticalSensorOffset(int angleoffset) {
        return String.format(":ANGLE_VERTICAL_SENSOR_OFFSET=%03x\n",angleoffset);
    }

    private String setupAngleDisplayOffset(int angleoffset) {
        return String.format(":ANGLE_DISPLAY_OFFSET=%03x\n",angleoffset);
    }

    private String setupMatrixSpeedLimit(int matrixspeedlimit) {
        return String.format(":SPEEDDISPLIMIT=%03x\n",matrixspeedlimit);
    }

    private String setupRealTimeWheelParams(int wheelparamperiod) {
        if ((wheelparamperiod==0)&&(btmi!=null)) btmi.showWheelInfo(null);
        return String.format(":SHOW_WHEEL_PARAM=%02x\n",wheelparamperiod);
    }

    public String setupAtConnection(Context context) {
	    	SharedPreferences sharedPrefs = PreferenceManager
	                .getDefaultSharedPreferences(context);
            String cmd="";
            cmd+= setupAngleDisplayOffset(sharedPrefs.getInt("AngularDisplayOffset", 0) / 10);
            cmd+=setupAngleVerticalSensorOffset(sharedPrefs.getInt("AngularVerticalSensorOffset", 0) / 10);
            cmd+=setupMatrixSpeedLimit(sharedPrefs.getInt("MatrixSpeedLimit",5)/10);
            cmd+=setupRealTimeWheelParams(sharedPrefs.getInt("RealTimeParamsPeriod",0)/10);
            return cmd;
    }

    private String mConnectedDeviceName = null;

    MyBlueToothMngInterf btmi;


    public void onCreate() {

        if(D) Log.e(TAG, "+++ ON CREATE +++");
        contactToBluetoothAdapter();

    }

    private void contactToBluetoothAdapter() {
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            //Toast.makeText(act, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            //BluetoothState=BLS_BluetoothService.STATE_NOT_AVAILABLE;
            if (btmi!=null) btmi.int_bluetooth_device_not_available();
            //if (mBluetoothAdapter==null) enableDeviceConnectionCBP.setEnabled(false);
            //finish();
            return;
        }
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");

        // Initialize the BLS_BluetoothService to perform bluetooth connections
        bluetoothService = new BluetoothService( mHandler);

    }


    private boolean firstTimeTryBlueToothEnable=false;
    public void onStart(/*Activity activity*/) {

        if(D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (mBluetoothAdapter==null) return;
        if (!mBluetoothAdapter.isEnabled()) {
            //Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            //activity.startActivityForResult(enableIntent, REQUEST_ENABLE_BT);

            if (btmi!=null) {
                if (! firstTimeTryBlueToothEnable)  btmi.ask_for_bluetooth_enabled();
                firstTimeTryBlueToothEnable=true;
            }
            // Otherwise, setup the chat session
        } else {
            if (bluetoothService == null) setupChat();
        }
    }


    public synchronized void onResume() {

        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (bluetoothService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (bluetoothService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth chat services
                bluetoothService.start();
            }
        }
    }


    public void onDestroy() {

        // Stop the Bluetooth chat services
        if (bluetoothService != null) bluetoothService.stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");
    }


    private void ensureDiscoverable(/*Activity activity*/) {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter==null) return;
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            /*Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            activity.startActivity(discoverableIntent);*/
            if (btmi!=null) btmi.ask_for_ensure_discoverable();
        }
    }

    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if ((bluetoothService ==null)||(bluetoothService.getState() != BluetoothService.STATE_CONNECTED)) {
            //Toast.makeText(activity, R.string.not_connected, Toast.LENGTH_SHORT).show();
            if (btmi!=null) btmi.int_not_connected();
            return;
        }

       // Log.i(TAG,"Send :"+message);

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BLS_BluetoothService to write
            byte[] send = message.getBytes();
            bluetoothService.write(send);

        }
    }


    private void refreshBluetoothPropertyButtons() {
        if (bluetoothSelectDevice!=null)
            bluetoothSelectDevice.setEnabled((BluetoothFeatureState== BluetoothService.STATE_BT_ON)
                                          &&((BluetoothConnectionState== BluetoothService.STATE_NONE)
                                            || (BluetoothConnectionState== BluetoothService.STATE_LISTEN)));
        if (enableDeviceConnectionCBP!=null) {
            enableDeviceConnectionCBP.setEnabled((BluetoothFeatureState== BluetoothService.STATE_BT_ON)
                    &&((BluetoothConnectionState== BluetoothService.STATE_NONE)
                    || (BluetoothConnectionState== BluetoothService.STATE_LISTEN)
                    || (BluetoothConnectionState== BluetoothService.STATE_CONNECTED)));
            enableDeviceConnectionCBP.setChecked(BluetoothConnectionState== BluetoothService.STATE_CONNECTED);
        }

        if (activateBluetoothCBP!=null) {
            activateBluetoothCBP.setEnabled((BluetoothFeatureState== BluetoothService.STATE_BT_ON)
                                            ||(BluetoothFeatureState== BluetoothService.STATE_BT_OFF));
            activateBluetoothCBP.setChecked((BluetoothFeatureState== BluetoothService.STATE_BT_ON)
                    ||(BluetoothFeatureState== BluetoothService.STATE_BT_TURNING_ON));
        }
        boolean connectionok=(BluetoothFeatureState== BluetoothService.STATE_BT_ON)&&(BluetoothConnectionState== BluetoothService.STATE_CONNECTED);
        if (angularDisplayOffset !=null) angularDisplayOffset.setEnabled(connectionok);
        if (angularVerticalSensorOffset!=null) angularVerticalSensorOffset.setEnabled(connectionok);
        if (matrixSpeedLimit!=null) matrixSpeedLimit.setEnabled(connectionok);
        if (realTimeParamsPeriod!=null) realTimeParamsPeriod.setEnabled(connectionok);
        if ((btmi!=null)&&(BluetoothConnectionState!= BluetoothService.STATE_CONNECTED)) btmi.showWheelInfo("");
    }

    // The Handler that gets information back from the BLS_BluetoothService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothService.MESSAGE_STATE_CHANGE:
                    if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    BluetoothConnectionState=msg.arg1;
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            if (btmi!=null)
                            {
                                btmi.int_connected(mConnectedDeviceName);
                                setupAtConnection(btmi.getContext());
                            }
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            if (btmi!=null) btmi.int_connecting();
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            if (btmi!=null) btmi.int_not_connected();
                            break;
                    }
                    refreshBluetoothPropertyButtons();
                    break;
                case BluetoothService.MESSAGE_BT_ACTIVATION_STATE_CHANGE:
                   // , STATE_BT_OFF, -1).sendToTarget();
                    BluetoothFeatureState=msg.arg1;
                    switch (msg.arg1) {
                        case BluetoothService.STATE_BT_OFF:
                            break;
                        case BluetoothService.STATE_BT_TURNING_OFF:
                            break;
                        case BluetoothService.STATE_BT_ON:
                            break;
                        case BluetoothService.STATE_BT_TURNING_ON:
                            break;
                    }
                    refreshBluetoothPropertyButtons();
                    break;
                case BluetoothService.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    break;
                case BluetoothService.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    drr.treatChar(readMessage.toCharArray());
                    break;
                case BluetoothService.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(BluetoothService.DEVICE_NAME);
                    if (btmi!=null) btmi.int_connected(mConnectedDeviceName);
                    break;
                case BluetoothService.MESSAGE_TOAST:
                    if (btmi!=null)  btmi.message(msg.getData().getString(BluetoothService.TOAST));
                    break;
            }
        }
    };


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        mHandler.obtainMessage(BluetoothService.MESSAGE_BT_ACTIVATION_STATE_CHANGE, BluetoothService.STATE_BT_OFF, -1).sendToTarget();
                        if (Main.d) Log.i(TAG, "Disactivate bluetooth");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        mHandler.obtainMessage(BluetoothService.MESSAGE_BT_ACTIVATION_STATE_CHANGE, BluetoothService.STATE_BT_TURNING_OFF, -1).sendToTarget();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        mHandler.obtainMessage(BluetoothService.MESSAGE_BT_ACTIVATION_STATE_CHANGE, BluetoothService.STATE_BT_ON, -1).sendToTarget();
                        if (Main.d) Log.i(TAG, "Activate bluetooth");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        mHandler.obtainMessage(BluetoothService.MESSAGE_BT_ACTIVATION_STATE_CHANGE, BluetoothService.STATE_BT_TURNING_ON, -1).sendToTarget();
                        break;
                }
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case BluetoothService.REQUEST_CONNECT_DEVICE:
                // When BLS_DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

                    // Attempt to connect to the device
                    bluetoothService.connect(device);
                    if (bluetoothSelectDevice!=null) bluetoothSelectDevice.setDevice(address,device.getName());
                }
                break;
            case BluetoothService.REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                    if (enableDeviceConnectionCBP !=null) enableDeviceConnectionCBP.setEnabled(true);
                } else {
                    if (btmi!=null) btmi.int_bluetooth_cannot_be_enabled();
                    if (enableDeviceConnectionCBP !=null) enableDeviceConnectionCBP.setEnabled(false);
                }
        }
    }


	public void sendData(String data) {
        sendMessage(data);

	}
	
}
